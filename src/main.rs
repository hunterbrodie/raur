use std::env;
use std::io;
use std::io::{BufReader, BufRead};
use std::fs;
use std::fs::File;
use std::process::{Command, Stdio};
use std::path::Path;

/*TODO
 *Write upgrade function
 *Add sudo support
 *Refactor code
 */

fn main()
{
    let args: Vec<String> = env::args().collect();

    if args.len() == 3
    {
        pacman_package(&args[1], &args[2])
    }
    else if args.len() == 2
    {
        pacman(&args[1]);
    }
    else if args.len() == 1
    {
        println!("raur: yet another wrapper for pacman to access the AUR written in Rust.")
    }
    else
    {
        println!("Failed to read command");
    }
}

fn install_package(name: &String)
{
    if pacman_available(&name)
    {
        Command::new("doas")
                .arg("pacman")
                .arg("-S")
                .arg(name)
                .status()
                .expect("pacman failed to start");
    }
    else
    {
        //Clone the git repository from the AUR
        let dir = format!("/tmp/{}", name);
        if Path::new(&dir).exists()
        {
            fs::remove_dir_all(&dir).unwrap();
        }
        fs::create_dir(&dir)
            .expect("Can't create folder");
        let repository = format!("https://aur.archlinux.org/{}.git", name);
        Command::new("git")
                .arg("clone")
                .arg(repository)
                .arg(&dir)
                .status()
                .expect("git failed to start");
    
        //Read and format dependencies
        let input = File::open(format!("{}/PKGBUILD", &dir)).expect("Can't read PKGBUILD");
        let buffered = BufReader::new(input);
    
        let mut depends: Vec<String> = Vec::new();
        let mut opt_depends: Vec<String> = Vec::new();
        let mut make_depends: Vec<String> = Vec::new();
    
        let mut check = 'f';
        for line in buffered.lines()
        {
            let line = line.unwrap();
    
            if line.starts_with("depends=(") || line.starts_with("depends_x86_64=(")
            {
                check = 'd';
            }
            else if line.starts_with("optdepends=(")
            {
                check = 'o';
            }
            else if line.starts_with("makedepends=(")
            {
                check = 'm';
            }
    
            if !check.eq(&'f')
            {
                match check
                {
                    'd' =>  depends.extend(line.split_whitespace()
                                                .map(|p| p.to_string().replace("depends=(", "").replace("depends_x86_64=(", "").replace("\'", "").replace(")", ""))
                                                .collect::<Vec<String>>()),
                    'o' =>  {
                                let mut line = line.split_whitespace().collect::<Vec<_>>();
                                line.retain(|p| p.contains(":"));
                                opt_depends.extend(line.iter()
                                                        .map(|p| p.to_string().replace("optdepends=(", "").replace("\'", "").replace(")", "").replace(":", ""))
                                                        .collect::<Vec<String>>());
                            },
                    'm' =>  make_depends.extend(line.split_whitespace()
                                                    .map(|p| p.to_string().replace("makedepends=(", "").replace("\'", "").replace(")", ""))
                                                    .collect::<Vec<String>>()),
                    _ => ()
                }
                if line.contains(")")
                {
                    check = 'f';
                }
            }
        }
    
        //Check what dependencies are installed or available through pacman
        depends.retain(|p| !installed(p) && !pacman_available(p));
        opt_depends.retain(|p| !installed(p) && !pacman_available(p));
        make_depends.retain(|p| !installed(p) && !pacman_available(p));
    
        //Ask what optional dependencies should be installed
        if opt_depends.len() > 0
        {
            println!("Optional Dependencies:");
    
            let mut deps = String::new();
            for i in 0..opt_depends.len()
            {
                deps.push_str(format!("{}.{} ", i + 1, opt_depends[i].clone()).as_str());
            }
    
            println!("{}", deps);
            println!("Enter as comma seperated list, [A]ll, or [N]one");
            let mut command = String::new();
            io::stdin()
                .read_line(&mut command)
                .expect("Failed to read input");
    
            match command.trim().to_lowercase().as_str()
            {
                "a" => (),
                "n" => opt_depends = Vec::new(),
                _ =>{
                        let command = command.split(',').map(|n| n.trim().parse::<usize>().expect("Failed to read input")).rev();
                        let mut tmp: Vec<String> = Vec::new();
                        for i in command
                        {
                            tmp.push(opt_depends[i - 1].clone());
                        }
                        opt_depends = tmp;
                    }
            }
        }
    
        //Make packages recursively
        for p in &make_depends
        {
            install_package(&p);
        }
        for p in &depends
        {
            install_package(&p);
        }
        for p in &opt_depends
        {
            install_package(&p);
        }
    
        //Make package
        env::set_current_dir(&dir).expect("Failed to change directory");
        Command::new("makepkg")
                .arg("-si")
                .status()
                .expect("Could not build package");
    }
}

fn installed(package: &String) -> bool
{
    Command::new("pacman")
                    .stdout(Stdio::null())
                    .stderr(Stdio::null())
                    .arg("-Qi")
                    .arg(package)
                    .status()
                    .expect("pacman failed to start")
                    .success()
}

fn pacman_available(package: &String) -> bool
{
    Command::new("pkgfile")
                    .stdout(Stdio::null())
                    .stderr(Stdio::null())
                    .arg("-l")
                    .arg(package)
                    .spawn()
                    .expect("pkgfile failed to start")
                    .wait()
                    .expect("pkgfile failed to start")
                    .success()
}

fn pacman(flag: &String)
{
    if flag.eq("-Syu")
    {
        
    }
    Command::new("doas")
            .arg("pacman")
            .arg(flag)
            .status()
            .expect("pacman failed to start");
}

fn pacman_package(flag: &String, package: &String)
{
    if flag.eq("-S")
    {
        install_package(package)
    }
    else if flag.eq("-Syu")
    {

    }
    else
    {
        Command::new("doas")
                .arg("pacman")
                .arg(flag)
                .arg(package)
                .status()
                .expect("pacman failed to start");
    }
}
